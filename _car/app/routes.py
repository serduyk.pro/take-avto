# app/routes.py
from flask import Blueprint, render_template

main_routes = Blueprint('main', __name__)

# Страница "Главная"
@main_routes.route("/base")
def base():
    return render_template('base.html')

@main_routes.route("/index")
def index():
    return render_template('index.html')

@main_routes.route("/notification")
def notification():
    return render_template('notification.html')

@main_routes.route("/guestion")
def guestion():
    return render_template('guestion.html')

@main_routes.route("/viev")
def viev():
    return render_template('viev.html')