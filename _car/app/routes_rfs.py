# app/routes.py
from flask import Blueprint, render_template, request

rfs_routes = Blueprint('rfs_routes', __name__)

# Страница "Запись"
@rfs_routes.route("/create", methods=['POST', 'GET'])
def create():
    if request.method == 'POST':
        # Получаем текущее время и время через час
        current_time = datetime.now()
        future_time = current_time + timedelta(hours=1)

        # Проверка наличия записей на текущее время и через час
        existing_records = Registration_for_services.query.filter(Registration_for_services.dateservice.between(current_time, future_time)).all()
        if existing_records:
            return print(existing_records)
        
        name = request.form['name']
        number = request.form['number']
        mail = request.form['mail']
        service = request.form['service']
        dateservice = datetime.strptime(request.form['dateservice'],
                                        '%Y-%m-%dT%H:%M')
        
        # Проверка времени записи
        if dateservice < future_time:
            return "Извините, запись возможна не раньше чем через час."

        client = Registration_for_services(name=name, number=number, mail=mail,
                        service=service, dateservice=dateservice)

        try:
            db.session.add(client)
            db.session.commit()
            return redirect('/successfully')

        except Exception as e:
            return (str(e))

    else:
        return render_template('create.html')

# Страница "Вы успешно записанны"
@rfs_routes.route("/successfully")
def successfully():
    return render_template('successfully.html')